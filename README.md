# README #

Repositorio  base integración de pruebas funcionales con Selenium en procesos CI/CD

### Objetivos ###

* En este repositorio se incluye código y documentación para la automatización de pruebas funcionales integradas con procesos CI/CD mediante el uso de Selenium, con las siguientes características:
- Configuración externalizada de las pruebas, a nivel global y particular de cada caso de test, a través de ficheros de propiedades y hojas en formato XLS para los datos de prueba
 - Uso de Selenium Hub para la ejecución remota de pruebas
 - Posibilidad de volcado automático de resultados a las Herramientas de gestión de pruebas funcionales (JIRA Zephyr)


### Automatización de las pruebas con Selenium ###

* Se ha optado por Selenium por ser herramienta de automatización de pruebas más utilizada en ámbito open source para aplicaciones basadas en la web. Dispone de un lenguaje específico de dominio para pruebas para escribir pruebas en un amplio número de lenguajes de programación populares incluyendo Java.
* Se utiliza el componente Selenium WebDriver, utilizando su API para desarrollar scripts que funcionan en los principales navegadores y están soportados por los principales fabricantes.
* Así mismo, se utiliza el componente Selenium Grid para ejecutar WebDriver remotamente y en diferentes navegadores, dando la posibilidad de ejecutar las pruebas en paralelo.
* Las imágenes de Selenium Grid utilizadas se han descargado de DockerHub:
  - docker pull selenium/hub
  - docker pull selenium/node-firefox
  - docker pull selenium/node-chrome

* Ejecución de Selenium Grid y Selenium nodes
  - Ejecución en entorno local (para pruebas y depuración)
  Para la ejecución contenerizada con Docker se ejecutan los siguientes comandos:
    docker run -d -p 4444:4444 -–name selenium-hub selenium/hub
    docker run -d –-link selenium-hub:hub selenium/node-chrome
    docker run -d –-link selenium-hub:hub selenium/node-firefox
    - Ejecución en entorno OpenShift
  Para la ejecución en un cluster de OpenShift .......
  TODO PENDIENTE DESCRIBIR

  ### Configuración y parametrización de las pruebas ###

  * El componente desarrollado permite configurar la ejecución de las pruebas mediante dos tipos de ficheros:
    - Fichero global de propiedades (por ejemplo, en el repositorio se incluye el fichero data/mytest.properties)
    - Fichero XLS de datos de test (por ejemplo, en el repositorio se incluye el fichero data/DATOS_TEST_1.xlsx)

  * Fichero Global de propiedades: Define las siguientes propiedades:
    - XLSfilename: Path del fichero de configuración de datos de Test (descrito más adelante)
    - Datos de configuración para enviar datos a JIRA-Zephyr:
      + zephyrIntegration:[YES/NO] Si su valor es "YES", el resultado de ejecución de cada prueba es notificado a Zephyr
      + zephyrBaseURL: URL base de JIRA Zephr (http://localhost:8080)
      + zephyrUsername: Usuario de acceso a Zephyr
      + zephyrUserpwd: Contraseña de accceso
      + zephyrProjectKey: Valor del PorjectKey sobre el que se ejecutan las pruebas
      + zephyrCycleId: Ciclo de pruebas sobre el eque se ejecutan las pruebas ()-1 para no definir un ciclo concreto)
    - Datos de configuración para ejecutar las pruebas a través de Selenium Grid:
      + webdriver_URL: URL de acceso al hub de Selenium Grid (http://localhost:4444/wd/hub)

  * Fichero XLS con datos de test: Incluye los datos a utiliza en los tests. Se trata de una hoja en formato Excel, en la que cada pestaña o tab incluye datos de un grupo de funcionalidades a probar. Desde el código del caso de test, se puede acceder a los datos de la plantilla XLS, en base a tres coordenadas:
    - Tab o pestaña: nombre de la pestaña en la que buscar los Datos
    - testName: identifica la fila (cada fila corresponde a un test)
    - nombre_del_campo: identifica la columna de la que extraer el Valor
    De este modo, se facilita que los datos de prueba, de las validaciones y otros datos (como por ejemplio el TestKey en Zephyr) se pueda definir en un fichero XLS configurable de manera externa al código fuente de la automatización.

    ### Volcado de resultados a JIRA Zephyr ###
    Cuando el fichero global de configuración tiene la propiedad zephyrIntegration=YES, se notificará a Zephyr el resultado de ejecución de cada test.
La comunicación con Zephyr se realiza mediante la clase TMConnectorZephyr, que invoca a la API REST de JIRA-Zephyr.
    Para poder vincular cada caso automatizado con el correpondiente TestCase definido en Zephyr, en el fichero se añade la columna "XLSzephyrTestcaseKey" que en cada fila (para cada test-case) indica el Test-Key correspondiente definido en Zephyr. De este modo, tras ekecuta el test, crea una nueva ejecución en JIRA-Zephyr.
    Para notificar a Zephyr, se utiliza su API, a través de estos servicios:
    - Crear una nueva ejecución (POST /rest/zapi/latest/execution): Método createExecution.- Se pasa como argumento  projectId, cycleId and issueId (testId)
    - Asignar el resultado de ejecución de un test (PUT /rest/zapi/latest/execution/<<cycle_XXX>>/execute): Método setTestResult, en el que se indica el cycleId en la URL y en el body se indica el estado de ejecución (PASSED/FAILED)
    - Obtener el id del proyecto a partir de su Key (GET /rest/api/latest/project/<<projectKey>>)
    - Obtener el id del TestCase a partir de su Key (GET /rest/api/latest/issue/<<testKey>>)

    ### Arranque del proceso ###
    El proceso arranca invocando al jar que contiene los scripts.
<<<<<<< HEAD
    java -Dproperties_file="d://mytest.properties" -classpath D:\Selenium\selenium-server-standalone-3.141.59.jar;D:\Eclipse\eclipse-workspace\WebdriverTest\target\WebdriverTest-0.0.1-SNAPSHOT-jar-with-dependencies.jar testlauncher.seleniumtest.TestLauncher
=======
>>>>>>> 45fcf30897ccadca397f7f89d7388ee4056497d7
    java -Dproperties_file="d://mytest.properties" -classpath D:\Eclipse\eclipse-workspace\WebdriverTest\target\WebdriverTest-0.0.1-SNAPSHOT-jar-with-dependencies.jar testlauncher.seleniumtest.TestLauncher


    El proceso incluye las siguientes dependencias:
    - org.seleniumhq.selenium - selenium-java - 3.141.59 			
    - org.apache.poi - poi - 3.7 (Apache POI para procesar datos XLS)
    - org.apache.poi - poi-ooxml - 3.7
    - org.apache.httpcomponents - httpclient - 4.5.10 (para acceder por http a la API de Zephyr para notificar resultados)
    - org.json - json - 20190722 (para procesar respuesta JSON de Zephyr)
    - org.slf4j - slf4j-api - 1.7.25 (para el logging)
    - ch.qos.logback - logback-classic - 1.2.3 (logging)


