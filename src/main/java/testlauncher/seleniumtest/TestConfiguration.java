package testlauncher.seleniumtest;

import java.lang.invoke.MethodHandles;
import java.util.Properties;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import testlauncher.util.TMConnectorZephyr;

public class TestConfiguration {
	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());
	
	private Properties testGlobalProperties = null;
	private XSSFWorkbook xlsDatafile = null;
	private TMConnectorZephyr zephyrConnector = null;
	
	public TestConfiguration (Properties testGlobalProperties, XSSFWorkbook xlsDatafile, TMConnectorZephyr zephyrConnector) {
		this.testGlobalProperties = testGlobalProperties; 
		this.xlsDatafile = xlsDatafile; 
		this.zephyrConnector = zephyrConnector;
	}
	
	public Properties getTestGlobalProperties() {
		return this.testGlobalProperties;
	}

	public XSSFWorkbook getXlsDatafile() {
		return this.xlsDatafile;
	}

	public TMConnectorZephyr getZephyrConnector() {
		return this.zephyrConnector;
	}


}
