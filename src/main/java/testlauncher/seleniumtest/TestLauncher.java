package testlauncher.seleniumtest;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import testlauncher.util.TMConnectorZephyr;

public class TestLauncher {
	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());
	
	
	
    public static void main(String[] args) {
    	Properties testGlobalProperties = null;
    	XSSFWorkbook xlsDatafile = null;
    	TMConnectorZephyr zephyrConnector = null;
    	boolean success = false;
    	try {
	    	//System.out.println("Starting ...");
	    	logger.info("Starting ...");
	    	String propertiesFilename =System.getProperty("properties_file");
	    	
	    	// Read global properties file
	    	logger.info("Reading properties file [" + propertiesFilename + "] ...");
	    	testGlobalProperties = testlauncher.util.ConfigReader.readConfigurationFile(propertiesFilename);
	    	
	    	// Read XLS Test data file
	    	String XLSfilename = testGlobalProperties.getProperty("XLSfilename");
	    	logger.info("Reading XLS file [" + XLSfilename + "] ...");
	    	xlsDatafile = testlauncher.util.XLSReader.createWB (XLSfilename);
	    	
	    	// Configure Zephyr connector
	    	String zephyrIntegration = testGlobalProperties.getProperty("zephyrIntegration");
	    	if ((zephyrIntegration!= null) && (zephyrIntegration.equals("YES"))) {
	    		logger.info("Zephyr connector is enabled. Test results will be notified to Zephyr.");
		    	logger.info("Reading Zephyr connector configuration ...");
		    	String zephyrBaseURL = testGlobalProperties.getProperty("zephyrBaseURL");
		    	String zephyrUsername = testGlobalProperties.getProperty("zephyrUsername");
		    	String zephyrUserpwd = testGlobalProperties.getProperty("zephyrUserpwd");
		    	String zephyrProjectKey = testGlobalProperties.getProperty("zephyrProjectKey");
		    	String zephyrCycleId = testGlobalProperties.getProperty("zephyrCycleId");
		    	zephyrConnector = new TMConnectorZephyr(zephyrBaseURL, zephyrUsername, zephyrUserpwd, zephyrProjectKey, zephyrCycleId);
	    	}
	    	else {
	    		logger.info("Zephyr connector is disabled. Test results wont be notified to Zephyr.");
	    	}
	    	
	    	// Create global configuration object
	    	TestConfiguration testGlobalConfiguration = new TestConfiguration(testGlobalProperties, xlsDatafile, zephyrConnector);
	    	
	    	// Execute test cases
			success = true;
			//DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			//TestCase2_HUB.execute(testGlobalConfiguration);
			success = success && Testcase_POC_1_Test_1_home.execute(testGlobalConfiguration, capabilities);
			capabilities = DesiredCapabilities.chrome();
			success = success && Testcase_POC_1_Test_1_home.execute(testGlobalConfiguration, capabilities);
			capabilities = DesiredCapabilities.firefox();
			success = success && Testcase_POC_1_Test_2_pettypes.execute(testGlobalConfiguration, capabilities);
			capabilities = DesiredCapabilities.chrome();
			success = success && Testcase_POC_1_Test_2_pettypes.execute(testGlobalConfiguration, capabilities);


    	} // end try
    	catch (IOException e) {
    		logger.error("ERROR IOException!" + e.getMessage());
			success = false;
		}
    	catch (Exception e) {
    		logger.error("ERROR Exception!" + e.getMessage());
			success = false;
    	}
		if (success) {
			logger.info("TESTS EXECUTION OK");
		}
		else{
			logger.info("TESTS EXECUTION KO");
		}
    }

}
