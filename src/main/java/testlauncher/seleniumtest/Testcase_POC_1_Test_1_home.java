package testlauncher.seleniumtest;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import testlauncher.util.TMConnectorZephyr;
import testlauncher.util.XLSReader;

//comment the above line and uncomment below line to use Chrome
import org.openqa.selenium.chrome.ChromeDriver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Testcase_POC_1_Test_1_home {

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

    public static boolean execute(TestConfiguration testGlobalConfiguration, DesiredCapabilities capabilities) {

        //System.out.println("Starting Test Execution ...");
        logger.info("Starting Test Execution ...");

        boolean success = true;
        // declaration and instantiation of objects/variables
        //System.setProperty("webdriver.gecko.driver","d:\\selenium\\geckodriver.exe");
        //WebDriver driver = new FirefoxDriver();
        //comment the above 2 lines and uncomment below 2 lines to use Chrome
        //System.setProperty("webdriver.chrome.driver","d:\\\\selenium\\\\chromedriver.exe");
        WebDriver driver = null;
        try {

            //DesiredCapabilities capabilities = DesiredCapabilities.chrome();

            capabilities.setCapability("version", "");
            capabilities.setPlatform(Platform.LINUX);

            // Read test data
            logger.info("Reading Test data ...");
            String tabName = "POC_1_Test";
            String testName = "POC_1_Test_1_home";
            String baseUrl = XLSReader.getParameter(testGlobalConfiguration.getXlsDatafile(), tabName, testName, "baseUrl");
            String expectedText = XLSReader.getParameter(testGlobalConfiguration.getXlsDatafile(), tabName, testName, "expectedText");

            logger.info("Starting Remote Web Driver ...");
            String webdriver_URL = testGlobalConfiguration.getTestGlobalProperties().getProperty("webdriver_URL");
            logger.info("Connecting to Selenium Hub at [" + webdriver_URL + "] ...");
            driver = new RemoteWebDriver(new URL(webdriver_URL), capabilities);

            logger.info("Starting browser ...");
            // launch Browser and direct it to the Base URL
            driver.get(baseUrl);
            logger.info("Checking response ...");

            // get the actual value of the title
            //String currentTitle = driver.getTitle();
            String pageSource = driver.getPageSource();
            String testStatusResult = TMConnectorZephyr.TEST_WIP;
            //if (currentTitle.contentEquals(expectedTitle)) {
            if (pageSource.indexOf(expectedText) >= 0) {
                logger.info("Test " + testName + " Passed!");
                testStatusResult = TMConnectorZephyr.TEST_PASSED;
            } else {
                success = false;
                logger.info("Test " + testName + " Failed!");
                testStatusResult = TMConnectorZephyr.TEST_FAILED;
            }

            // notify results to Zephyr
            if (testGlobalConfiguration.getZephyrConnector() != null) {
                // Read Zephyr related data
                String testCaseKey = XLSReader.getParameter(testGlobalConfiguration.getXlsDatafile(), tabName, testName, "zephyrTestcaseKey");


                String cycleId = testGlobalConfiguration.getZephyrConnector().createExecution(testCaseKey);
                testGlobalConfiguration.getZephyrConnector().setTestResult(cycleId, testStatusResult);

            }

        } catch (RuntimeException e) {
            logger.error("ERROR Runtime!" + e.getMessage());
            success = false;
        } catch (MalformedURLException e) {
            logger.error("ERROR MalformedURLException!" + e.getMessage());
            success = false;
        } catch (IOException e) {
            logger.error("ERROR IOException!" + e.getMessage());
            e.printStackTrace();
            success = false;
        }

        //close browser
        if (driver != null) {
            logger.info("Closing SeleniumWeb driver ...");
            driver.quit();
            //driver.close();
            logger.info("SeleniumWeb driver closed.");
        }
        return success;

    }
}

