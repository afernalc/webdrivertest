package testlauncher.util;
import java.io.FileInputStream;
//import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
//import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.*;
//import org.apache.poi.xssf.usermodel.XSSFSheet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XLSReader {

	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

	public static XSSFWorkbook createWB (String namefile) throws InvalidFormatException, IOException {
        InputStream inp = new FileInputStream(namefile);
        XSSFWorkbook wb = (XSSFWorkbook) org.apache.poi.ss.usermodel.WorkbookFactory.create(inp);
        inp.close();
        return wb;
		
	}

	public static String getParameter (XSSFWorkbook wb, int sheetNumber, String rowName, String colName) {
		String result = null;
		try {
			 org.apache.poi.xssf.usermodel.XSSFSheet sht = wb.getSheetAt(sheetNumber);        
			 // Read header row looking for requested parameter.
			 int paramColumn = -1;
			 for (int i = 0; i < sht.getRow(0).getLastCellNum(); i++) {
			        if (sht.getRow(0).getCell(i).getStringCellValue()
			                      .equalsIgnoreCase(colName)) {
			               paramColumn = i;
			               break;
			        }
			 }            
			 if (paramColumn < 0) {
			        throw new Exception("Column (parameter name) not found in the parameters file.");
			 }
			
			 // Read TestID column looking for requested row (search in cols 1 and 2).
			 int testRow = -1;
			 for (int i = 0; i < sht.getLastRowNum() + 1; i++) {
				 
				 String X = sht.getRow(i).getCell(0).getStringCellValue();
				 
			        if (   (sht.getRow(i).getCell(0).getStringCellValue().equalsIgnoreCase(rowName))
			        	|| (sht.getRow(i).getCell(1).getStringCellValue().equalsIgnoreCase(rowName))) {
			               testRow = i;
			               break;
			        }
			 }            
			 if (testRow < 0) {
			        throw new Exception("Row (Test ID) not found in the parameters file. ");
			 }
			

			try {
			         result = sht.getRow(testRow).getCell(paramColumn).getStringCellValue();
			} catch (Exception e) {}
			if (result == null) {
				// Not String
				try {
					result = Integer.toString((int) sht.getRow(testRow).getCell(paramColumn).getNumericCellValue());
				} catch (Exception e) {
					throw new Exception("Value not found in the parameters file... ");
				}
			  }
			 
			 if (result == null) {
				 throw new Exception("Value not found in the parameters file.... "); 
			 }
			
			 
			 //logger.info("VALOR LEIDO:" + result);
	       
		} catch (Exception e) {
			logger.error("ERROR in XLS getParameter:" + e.getMessage());	
		}
		return result;
	}

	public static String getParameter (XSSFWorkbook wb, String sheetName, String rowName, String colName) {
		
		int sheetIndex = wb.getSheetIndex(sheetName);
		return getParameter(wb,sheetIndex,rowName,colName);
	}


}
