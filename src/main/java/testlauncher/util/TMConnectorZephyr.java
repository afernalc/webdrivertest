package testlauncher.util;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.codec.binary.Base64;

public class TMConnectorZephyr {

	public static final String TEST_PASSED = "1";
	public static final String TEST_FAILED = "2";
	public static final String TEST_WIP = "3";
	public static final String TEST_BLOCKED = "4";

	
	
	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

	private String authStr; //String to be used in request headers for authentication ("Authorization", "Basic + authStr")
	private String baseURL; // JIRA-Zephyr base URL
	private String zephyrProjectKey; // Zephyr Project Key
	private String zephyrProjectId; // Zephyr Project Id
	private String zephyrCycleId; // Zephyr TestCycle Id
	
	
	
	public TMConnectorZephyr (String baseURL, String username, String pwd, String zephyrProjectKey, String zephyrCycleId) throws IOException {
		this.baseURL = baseURL;
    	String authStr = username + ":" + pwd;
    	byte[] authAux = authStr.getBytes();
    	byte[] encryptedId = Base64.encodeBase64(authAux);
    	this.authStr= new String(encryptedId);
		this.zephyrProjectKey = zephyrProjectKey;
		this.zephyrProjectId = getProjectIdFromProjectKey(zephyrProjectKey);
		this.zephyrCycleId = zephyrCycleId;
		
	}
	
	// Create a Zephyr new execution of the test case (based on projectId, cycleId and issueId (testId))
	// Return clycleId
    public String createExecution(String testCaseKey) throws IOException {
    	// The new Test execution is defined by a POST request against Zephyr API
    	// POST /rest/zapi/latest/execution
    	// BODY:
    		  //"projectId": "10000",
    		  //"cycleId": "-1",
    		  //"issueId": "10024"

    	String newCycleId = null;
    	String testcaseId = getTestIdFromTestKey(testCaseKey);
	
    	String url = this.baseURL + "/rest/zapi/latest/execution";
    	
    	//System.out.println("URL:" + url);
    	
        HttpPost post = new HttpPost(url);
        
        post.addHeader("Content-Type", "application/json");
        post.addHeader("Authorization", "Basic " + this.authStr);
        
        logger.info("Creating execution for cycleId [" + this.zephyrCycleId + "] and issueId [" + testcaseId + "].");
        
        String  body = "{\"projectId\": \"" + this.zephyrProjectId + "\", \"cycleId\": \"" + this.zephyrCycleId + "\", \"issueId\": \"" + testcaseId + "\"}";
        //logger.info("BODY:" + body);
        post.setEntity(new StringEntity(body));
    	CloseableHttpClient httpClient = HttpClients.createDefault();
    	CloseableHttpResponse response = httpClient.execute(post);

    	String result = new BasicResponseHandler().handleResponse(response);
    	logger.info("result:" + body);
    	
    	JSONObject jsonObject = new JSONObject(result);
    	Iterator iterator  = jsonObject.keys();
        String key = (String)iterator.next();
        if (key != null) {
        	JSONObject jsonChildObject = ((JSONObject)jsonObject.get(key));
        	//logger.info("CHILD NODE:" + jsonChildObject.toString());
        	newCycleId = String.valueOf(jsonChildObject.getInt("id"));
        }
        else {
        	logger.error("Response to createExecution is empty");
        }
        
        logger.info("Readed clycleId  [" + newCycleId + "].");
        return newCycleId;
    }
	
	
	// Set the test execution on Zephyr (based on cycleId)
	// Return clycleId
    public String setTestResult(String cycleId, String testStatusResult) throws IOException {
    	// The new Test execution is defined by a POST request against Zephyr API
    	// POST /rest/zapi/latest/execution
    	// BODY:
    		  //"projectId": "10000",
    		  //"cycleId": "-1",
    		  //"issueId": "10024"
    	// STATES:
    	//1.- Passed
    	//2.- Failed
    	//3.- WIP
    	//4.- Blocked
    	
    	
    	//url = "http://localhost:8080/rest/zapi/latest/execution/2/execute";
    	
    	String url = this.baseURL + "/rest/zapi/latest/execution/" + cycleId + "/execute";
        HttpPut put = new HttpPut(url);
        
        put.addHeader("Content-Type", "application/json");
        put.addHeader("Authorization", "Basic " + this.authStr);
        
    	CloseableHttpClient httpClient = HttpClients.createDefault();
		logger.info("Updating Test satus in Zephyr... ");

        // Set a WorkInProgress State to ensure that Zephyr updates status and updates test date
        String  bodyTemp = "{\"status\": \""+ TEST_WIP +"\"}";
        put.setEntity(new StringEntity(bodyTemp));
		CloseableHttpResponse response = httpClient.execute(put);
        
        // And now, update test with the desired Status value 
        String  body = "{\"status\": \""+testStatusResult+"\"}";
        put.setEntity(new StringEntity(body));
		response = httpClient.execute(put);

		String result = EntityUtils.toString(response.getEntity());
		String httpCodeResult = response.toString();
		
		//logger.info("Zephyr Response to Update Test satus:" + result + "---" + httpCodeResult);
        	

        return result;
    }
    
	// get Zephyr projectId from a prokect Key
    public String getProjectIdFromProjectKey (String projectKey) throws IOException {
    	
    	String url = this.baseURL + "/rest/api/latest/project/" + projectKey;
        HttpGet getRequest = new HttpGet(url);
        
        getRequest.addHeader("Content-Type", "application/json");
        getRequest.addHeader("Authorization", "Basic " + this.authStr);
        
        logger.info("Reading projectId from Key [" + projectKey + "]: URL [" + url + "] ...");
        
    	CloseableHttpClient httpClient = HttpClients.createDefault();
    	CloseableHttpResponse response = httpClient.execute(getRequest);
    	String result = new BasicResponseHandler().handleResponse(response);
    	
        JSONObject jsonObject = new JSONObject(result);
        String projectId = jsonObject.getString("id");
        logger.info("Readed projectId  [" + projectId + "] from projectKey [" + projectKey + "].");
        
        return projectId;
    }    

	// get Zephyr testId from a testKey
    public String getTestIdFromTestKey (String testKey) throws IOException {
    	
    	String url = this.baseURL + "/rest/api/latest/issue/" + testKey;
        HttpGet getRequest = new HttpGet(url);
        
        getRequest.addHeader("Content-Type", "application/json");
        getRequest.addHeader("Authorization", "Basic " + this.authStr);
        
        logger.info("Reading projectId from Key [" + testKey + "]: URL [" + url + "] ...");
        
    	CloseableHttpClient httpClient = HttpClients.createDefault();
    	CloseableHttpResponse response = httpClient.execute(getRequest);
    	String result = new BasicResponseHandler().handleResponse(response);
    	
        JSONObject jsonObject = new JSONObject(result);
        String testId = jsonObject.getString("id");
        logger.info("Readed projectId  [" + testId + "] from projectKey [" + testKey + "].");
        
        return testId;
    }    
    
    
/*	try {
		String projectId = zephyrConnector.getProjectIdFromProjectKey(projectKey);
	
		logger.info("EL PROYCTO ID ES ... " + projectId);
	
	}
	catch (java.io.IOException e) {
		logger.error("ERROR IOException!" + e.getMessage());
	}    
*/    

}
